import React, { Component } from 'react';
import Book from './Book'

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: '',
      author: '',
      bookList: [
          {
            title: 'Book Name 1',
            author: 'Author 1'
          },
          {
            title: 'Book Name 2',
            author: 'Author 2'
          },
      ]
    }
    this.handleAddBook = this.handleAddBook.bind(this)
    this.handleTitleInputChange = this.handleTitleInputChange.bind(this)
    this.handleAuthorInputChange = this.handleAuthorInputChange.bind(this)
  }

  handleTitleInputChange(evt) {
    this.setState({...this.state, title: evt.target.value })
  }
  
  handleAuthorInputChange(evt) {
    this.setState({...this.state, author: evt.target.value })
  }
  
  handleAddBook() {
    const { bookList, title, author } = this.state
    this.setState({...this.state, bookList: [...bookList, {title, author}], title: '', author: ''})
  }

  render() {
    const { bookList, title, author } = this.state
    return (
      <div>
        <div>
          <input name="title" onChange={this.handleTitleInputChange} value={title} />
          <input name="author" onChange={this.handleAuthorInputChange} value={author} />
          <button onClick={this.handleAddBook}>Add Book</button>
        </div>
        Book List ({bookList.length})
        {
          bookList.map(book => <Book key={Math.random() * 100} data={book} />)
        }
      </div>
    );
  }
}

export default App;