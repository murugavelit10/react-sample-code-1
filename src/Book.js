import React, { Component } from 'react';

class Book extends Component {
    render() {
        const { title, author } = this.props.data
        return (
            <div>
                <div>{title}</div>
                <div>{author}</div>
            </div>
        );
    }
}

export default Book;